/**
 * @license Axis
 * (c) 2016 Planimeter.
 */
'use strict';

var express    = require('express');
var bodyParser = require('body-parser');

var app        = express();
module.exports = app;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

require('./routes/account');
require('./routes/save');
