/**
 * @license Axis
 * (c) 2016 Planimeter.
 */
'use strict';

var db     = require('./lib/database');
var crypto = require('crypto');
var app    = process.argv[2];

if (!app) {
  console.log('node generateAppSecret <app>');
  process.exit(1);
}

var appSecret = crypto.createHash('sha1');
appSecret     = appSecret.update(crypto.randomBytes(2048));
appSecret     = appSecret.digest('hex');

db.apps.findOne({
  name: app,
}, function (err, document) {
  if (err) {
    console.log(err);
  }

  if (document) {
    db.apps.update({
      _id: document._id,
    }, {
      $set: {
        appSecret: appSecret,
      },
    });
  } else {
    db.apps.insert({
      name:      app,
      appSecret: appSecret,
    });
  }

  db.close();
  console.log(appSecret);
});
