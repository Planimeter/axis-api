/**
 * @license Axis
 * (c) 2016 Planimeter.
 */
'use strict';

var db = require('./database');

module.exports = exports;

exports.getAppByAppSecret = function (appSecret, callback) {
  db.apps.findOne({
    appSecret: appSecret,
  }, function (err, document) {
    if (err) {
      callback(err, null);
      return;
    }

    if (!document) {
      callback(new Error('Not Found'), null);
      return;
    }

    callback(null, document);
  });
};
