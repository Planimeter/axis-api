/**
 * @license Axis
 * (c) 2016 Planimeter.
 */
'use strict';

var db = require('./database');

module.exports = exports;

exports.getSavedGame = function (username, appSecret, name, callback) {
  var self = this;
  self.getAccount(username, function (err, account) {
    if (err) {
      callback(err, null);
      return;
    }

    self.getAppByAppSecret(appSecret, function (err, app) {
      if (err) {
        callback(err, null);
        return;
      }

      db.saves.findOne({
        account: account._id,
        app:     app._id,
        name:    name,
      }, function (err, document) {
        if (err) {
          callback(err, null);
          return;
        }

        if (!document) {
          callback(new Error('Not Found'), null);
          return;
        }

        callback(null, document.save);
      });
    });
  });
};

exports.setSavedGame = function (username, appSecret, name, save, callback) {
  var self = this;
  self.getAccount(username, function (err, account) {
    if (err) {
      callback(err);
      return;
    }

    self.getAppByAppSecret(appSecret, function (err, app) {
      if (err) {
        callback(err);
        return;
      }

      db.saves.findOne({
        account: account._id,
        app:     app._id,
        name:    name,
      }, function (err, document) {
        if (err) {
          callback(err);
          return;
        }

        if (!document) {
          db.saves.insert({
            account: account._id,
            app:     app._id,
            name:    name,
            save:    save,
          });
        } else {
          db.saves.update({
            _id:     document._id,
            account: account._id,
            app:     app._id,
            name:    name,
          }, {
            $set: {
              save: save,
            },
          });
        }

        callback(null);
      });
    });
  });
};
