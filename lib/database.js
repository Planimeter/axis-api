/**
 * @license Axis
 * (c) 2016 Planimeter.
 */
'use strict';

var mongojs = require('mongojs');
var db      = mongojs('axis', [
  'accounts',
  'apps',
  'saves',
]);

db.on('error', function (err) {
  console.log(err);
});

module.exports = db;
