/**
 * @license Axis
 * (c) 2016 Planimeter.
 */
'use strict';

var AXIS_URL = 'http://www.planimeter.org/axis';

var validator  = require('validator');
var db         = require('./database');
var bcrypt     = require('bcrypt-nodejs');
var crypto     = require('crypto');
var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport();

var sendVerificationEmail = function (username, email, verificationCode) {
  transporter.sendMail({
    from: 'Axis <axis@andrewmcwatters.com>',
    to: email,
    subject: 'Welcome to Axis',
    html: 'Welcome to Axis, ' + username + '.<br> ' +
          '<a href="' + AXIS_URL + '/account?verify=' +
                        verificationCode + '">' +
            'Click here' +
          '</a> to verify your account.<br><br>' +

          'If you can\'t click on the link above, copy and paste this URL ' +
          'into your browser: ' +
          AXIS_URL + '/account?verify=' + verificationCode,
  });
};

var verificationCodes = {};

exports.createAccount = function (username, email, password, callback) {
  if (!validator.isEmail(email)) {
    callback(new Error('Invalid Email'));
    return;
  }

  var self = this;
  self.getAccount(username, function (err, account) {
    if (err && err.message !== 'Not Found') {
      callback(err);
      return;
    }

    if (account) {
      callback(new Error('Username Taken'));
      return;
    }

    var verificationCode = crypto.createHash('sha1');
    verificationCode     = verificationCode.update(crypto.randomBytes(20));
    verificationCode     = verificationCode.digest('hex');

    db.accounts.insert({
      username:         username,
      email:            email,
      password:         bcrypt.hashSync(password, bcrypt.genSaltSync()),
      verificationCode: verificationCode,
      creationDate:     new Date(),
    });

    sendVerificationEmail(username, email, verificationCode);
    verificationCodes[verificationCode] = setTimeout(function () {
      self.deleteAccount(username);
    }, 1000 * 60 * 5);

    callback(null);
  });
};

exports.verifyAccount = function (verificationCode, callback) {
  db.accounts.findOne({
    verificationCode: verificationCode,
  }, function (err, document) {
    if (err) {
      callback(err, null);
      return;
    }

    if (!document) {
      callback(new Error('Not Found'), null);
      return;
    }

    db.accounts.update({
      verificationCode: verificationCode,
    }, {
      $unset: {
        verificationCode: '',
      },
    });
    clearTimeout(verificationCodes[verificationCode]);
    delete verificationCodes[verificationCode];
    callback(null, {
      username: document.username,
    });
  });
};

function caseInsensitive(s) {
  return new RegExp(['^', s, '$'].join(''), 'i');
}

exports.getAccount = function (username, callback) {
  db.accounts.findOne({
    username: caseInsensitive(username),
  }, function (err, document) {
    if (err) {
      callback(err, null);
      return;
    }

    if (!document) {
      callback(new Error('Not Found'), null);
      return;
    }

    callback(null, document);
  });
};

exports.deleteAccount = function (username, callback) {
  db.accounts.remove({
    username: caseInsensitive(username),
  }, true, function (err, lastErrorObject) {
    if (!callback) {
      return;
    }

    if (err) {
      callback(err);
      return;
    }

    if (lastErrorObject.n !== 0) {
      callback(null);
    } else {
      callback(new Error('Not Found'));
    }
  });
};

exports.signin = function (username, password, callback) {
  this.getAccount(username, function (err, account) {
    if (err) {
      callback(err, null);
      return;
    }

    if (!bcrypt.compareSync(password, account.password)) {
      callback(new Error('Incorrect Password'), null);
      return;
    }

    if (account.verificationCode) {
      callback(new Error('Account Not Verified'), null);
      return;
    }

    var ticket = crypto.createHash('sha1');
    ticket     = ticket.update(crypto.randomBytes(20));
    ticket     = ticket.digest('hex');

    db.accounts.update({
      username: account.username,
      password: account.password,
    }, {
      $set: {
        ticket: ticket,
      },
    });

    account.ticket = ticket;
    delete account._id;
    delete account.password;
    callback(null, account);
  });
};

exports.signout = function (ticket, callback) {
  db.accounts.findOne({
    ticket: ticket,
  }, function (err, document) {
    if (err) {
      callback(err);
      return;
    }

    if (!document) {
      callback(new Error('Not Found'));
      return;
    }

    db.accounts.update({
      ticket: ticket,
    }, {
      $unset: {
        ticket: '',
      },
    });
    callback(null);
  });
};

exports.authenticate = function (ticket, callback) {
  db.accounts.findOne({
    ticket: ticket,
  }, function (err, document) {
    if (err) {
      callback(err, null);
      return;
    }

    if (!document) {
      callback(new Error('Not Found'), null);
      return;
    }

    delete document._id;
    delete document.password;
    callback(null, document);
  });
};
