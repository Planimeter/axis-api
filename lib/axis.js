/**
 * @license Axis
 * (c) 2016 Planimeter.
 */
'use strict';

var merge = require('utils-merge');

module.exports = exports;

merge(exports, require('./account'));
merge(exports, require('./app'));
merge(exports, require('./save'));
