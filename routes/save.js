/**
 * @license Axis
 * (c) 2016 Planimeter.
 */
'use strict';

var app  = require('../app');
var axis = require('../lib/axis');

function getSavedGame(req, res) {
  var username  = req.query.username;
  var appSecret = req.query.app_secret;
  var name      = req.query.name;

  if (!username || !appSecret) {
    res.sendStatus(400);
    return;
  }

  axis.getSavedGame(username, appSecret, name, function (err, save) {
    if (err) {
      switch (err.message) {
      case 'Not Found':
        res.sendStatus(404);
        break;
      default:
        res.status(500).send(err.message);
        break;
      }
      return;
    }

    res.json(save);
  });
}

app.get('/save', getSavedGame);

function setSavedGame(req, res) {
  var username  = req.body.username;
  var appSecret = req.body.app_secret;
  var name      = req.body.name;
  var save      = req.body.save;

  if (!username || !appSecret || !save) {
    res.sendStatus(400);
    return;
  }

  axis.setSavedGame(username, appSecret, name, save, function (err) {
    if (err) {
      switch (err.message) {
      case 'Not Found':
        res.sendStatus(404);
        break;
      default:
        res.status(500).send(err.message);
        break;
      }
      return;
    }

    res.sendStatus(200);
  });
}

app.post('/save', setSavedGame);
app.put('/save', setSavedGame);
