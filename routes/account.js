/**
 * @license Axis
 * (c) 2016 Planimeter.
 */
'use strict';

var app  = require('../app');
var axis = require('../lib/axis');
var cors = require('cors');

var whitelist = ['http://www.planimeter.org', 'http://localhost:9000'];
var corsOptions = {
  origin: function (origin, callback) {
    var originIsWhitelisted = whitelist.indexOf(origin) !== -1;
    callback(null, originIsWhitelisted);
  },
};

app.post('/account', function (req, res) {
  var username = req.body.username;
  var email    = req.body.email;
  var password = req.body.password;

  if (!username || !email || !password) {
    res.sendStatus(400);
    return;
  }

  axis.createAccount(username, email, password, function (err) {
    if (err) {
      switch (err.message) {
      case 'Invalid Email':
        res.status(400).send(err.message);
        break;
      case 'Username Taken':
        res.status(409).send(err.message);
        break;
      default:
        res.status(500).send(err.message);
        break;
      }
      return;
    }

    res.sendStatus(200);
  });
});

app.options('/verify', cors(corsOptions));

app.post('/verify', cors(corsOptions), function (req, res) {
  var verificationCode = req.body.verification_code;

  if (!verificationCode) {
    res.sendStatus(400);
    return;
  }

  axis.verifyAccount(verificationCode, function (err, account) {
    if (err) {
      switch (err.message) {
      case 'Not Found':
        res.sendStatus(404);
        break;
      default:
        res.status(500).send(err.message);
        break;
      }
      return;
    }

    res.json(account);
  });
});

app.options('/account', cors(corsOptions));

app.get('/account', cors(corsOptions), function (req, res) {
  var username = req.query.username;

  if (!username) {
    res.sendStatus(400);
    return;
  }

  axis.getAccount(username, function (err) {
    if (err) {
      switch (err.message) {
      case 'Not Found':
        res.sendStatus(404);
        break;
      default:
        res.status(500).send(err.message);
        break;
      }
      return;
    }

    res.sendStatus(200);
  });
});

app.options('/signin', cors(corsOptions));

app.post('/signin', cors(corsOptions), function (req, res) {
  var username = req.body.username;
  var password = req.body.password;

  if (!username || !password) {
    res.sendStatus(400);
    return;
  }

  axis.signin(username, password, function (err, account) {
    if (err) {
      switch (err.message) {
      case 'Account Not Verified':
        res.status(400).send(err.message);
        break;
      case 'Incorrect Password':
        res.status(403).send(err.message);
        break;
      case 'Not Found':
        res.status(404).send(err.message);
        break;
      default:
        res.status(500).send(err.message);
        break;
      }
      return;
    }

    res.json(account);
  });
});

app.options('/signout', cors(corsOptions));

app.post('/signout', cors(corsOptions), function (req, res) {
  var ticket = req.body.ticket;

  if (!ticket) {
    res.sendStatus(400);
    return;
  }

  axis.signout(ticket, function (err) {
    if (err) {
      switch (err.message) {
      case 'Not Found':
        res.sendStatus(404);
        break;
      default:
        res.status(500).send(err.message);
        break;
      }
      return;
    }

    res.sendStatus(200);
  });
});

app.options('/authenticate', cors(corsOptions));

app.post('/authenticate', cors(corsOptions), function (req, res) {
  var ticket = req.body.ticket;

  if (!ticket) {
    res.sendStatus(400);
    return;
  }

  axis.authenticate(ticket, function (err, account) {
    if (err) {
      switch (err.message) {
      case 'Not Found':
        res.sendStatus(404);
        break;
      default:
        res.status(500).send(err.message);
        break;
      }
      return;
    }

    res.json(account);
  });
});
